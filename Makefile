CC = gcc
INC = includes
LIBDIR = libft
LIBINC = $(LIBDIR)/includes
LIBARCH = libft.a
LIBNAME = ft
CFLAGS = -Wall -Werror -Wextra -I$(INC) -I$(LIBINC)
NAME = libdstring.a
SDIR = srcs
ODIR = objs
SRCS = ds_cat.c		\
		ds_cat_ds.c	\
		ds_dup.c		\
		ds_free.c		\
		ds_len.c		\
		ds_new.c		\
		ds_newlen.c

OBJ = $(SRCS:.c=.o)

all: $(NAME)

DEP:
	make -C $(LIBDIR)

$(NAME): $(ODIR) DEP $(addprefix $(ODIR)/, $(OBJ))
	ar -rcv $(LIBDIR)/$(LIBARCH) $(addprefix $(ODIR)/, $(OBJ)) 
	mv $(LIBDIR)/$(LIBARCH) $@
	ranlib $@

$(ODIR):
	mkdir $@

$(ODIR)/%.o: $(SDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	make fclean -C $(LIBDIR)
	rm -rf $(ODIR)

fclean: clean
	rm -f $(NAME)

re: fclean all
