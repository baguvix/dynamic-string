#include "dstring.h"
#include <stdio.h>

int		main()
{
	t_dstr	mystr, s;
	t_head	a;

	mystr = ds_new("Semen pitux");
	printf("%s,\tstrlen: %zu\n", mystr, ds_len(mystr));
	mystr = ds_cat(mystr, ", nu pryam fighting chiken!");
	printf("%s,\tstrlen: %zu\n", mystr, ds_len(mystr));
	s = ds_new("");
	printf("%s,\tstrlen: %zu\n", s, ds_len(s));
	s = ds_cat_ds(s, mystr);
	printf("%s,\tstrlen: %zu\n", s, ds_len(s));
	ds_free(s);
	printf("%s,\tstrlen: %zu\n", mystr, ds_len(mystr));
	s = ds_dup(mystr);
	printf("%s,\tstrlen: %zu\n", s, ds_len(s));
	ds_free(mystr);
	return (0);
}
