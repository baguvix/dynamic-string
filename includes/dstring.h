/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dstring.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 16:18:00 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/14 15:15:39 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef	DSTRING_H
# define DSTRING_H

# include <stdlib.h>
# include "libft.h"

typedef	char *		t_dstr;

typedef	struct		s_head
{
	size_t			memlen;
	size_t			strlen;
}					t_head;

t_dstr				ds_newlen(const char *content, size_t initlen);
t_dstr				ds_new(const char *content);
size_t				ds_len(t_dstr s);
void				ds_free(t_dstr s);
t_dstr				ds_dup(t_dstr s);
t_dstr				ds_cat_ds(t_dstr s1, const t_dstr s2);
t_dstr				ds_cat(t_dstr s, const char *str);

#endif
