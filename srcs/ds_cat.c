/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ds_cat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 16:40:28 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/14 14:33:36 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dstring.h>

t_dstr	ds_cat(t_dstr s, const char *str)
{
	t_head *head;
	t_dstr	ret;
	size_t	newlen;
	size_t	len;

	head = (t_head *)(s - sizeof(t_head));
	len = ft_strlen(str);
	newlen = head->memlen;
	while (head->strlen + len > newlen)
		newlen *= 2;
	ret = s;
	if (newlen > head->memlen)
	{
		ret = ds_newlen((char *)s, newlen);
		ds_free(s);
	}
	head = (t_head *)(ret - sizeof(t_head));
	ft_memcpy(ret + head->strlen, str, len + 1);
	head->strlen += len;
	return (ret);
}
