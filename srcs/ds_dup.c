/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ds_dup.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 19:43:16 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/14 15:04:02 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "dstring.h"

t_dstr	ds_dup(t_dstr s)
{
	void	*new;
	t_head	*head;
	size_t	size;

	head = (t_head *)(s - sizeof(t_head));
	size = sizeof(t_head) + head->memlen;
	new = malloc(size + 1);
	ft_memcpy(new, head, size + 1);
	return ((t_dstr)(new + sizeof(t_head)));
}
