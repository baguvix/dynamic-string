/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ds_cat_ds.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 19:50:22 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/14 14:47:29 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "dstring.h"

t_dstr		ds_cat_ds(t_dstr s1, const t_dstr s2)
{
	t_head	*head1;
	t_head	*head2;
	t_dstr	ret;
	
	head1 = (t_head *)(s1 - sizeof(t_head));
	head2 = (t_head *)(s2 - sizeof(t_head));
	ret = s1;
	if (head1->strlen + head2->strlen > head1->memlen)
	{
		ret = ds_newlen((char *)s1, head1->memlen + head2->memlen);
		ds_free(s1);
	}
	ft_memcpy(ret + head1->strlen, s2, head2->strlen + 1);
	head1 = (t_head *)(ret - sizeof(t_head));
	head1->strlen += head2->strlen;
	return (ret);
}
