/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ds_newlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 16:36:32 by orhaegar          #+#    #+#             */
/*   Updated: 2019/07/23 19:03:15 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "dstring.h"

t_dstr	ds_newlen(const char *content, size_t initlen)
{
	t_head	head;
	void	*new;

	head.strlen = ft_strlen(content);
	if (head.strlen > initlen)
		head.strlen = initlen;
	head.memlen = initlen;
	new = malloc(sizeof(t_head) + head.memlen + 1);
	if (new == NULL)
		return (NULL);
	ft_memcpy(new, &head, sizeof(t_head));
	ft_memcpy(new + sizeof(t_head), content, head.strlen + 1);
	return (t_dstr)(new + sizeof(t_head));
}
